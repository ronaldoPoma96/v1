package com.ronaldo.techgap.repository;

import com.ronaldo.techgap.entity.Task;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class TaskRepositoryTests {

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void TaskRepository_save_ReturnSavedTask(){

        Task taskTest = Task.builder().taskName("Domanda di test").build();
        Task taskTest2 = Task.builder().taskName("Domanda di test 2").build();

        Task savedTask = taskRepository.save((taskTest));
        Task savedTask2 = taskRepository.save((taskTest2));

        Assertions.assertThat(savedTask).isNotNull();
        Assertions.assertThat(savedTask.getId()).isGreaterThan(0);
        Assertions.assertThat(savedTask.getId()).isEqualTo(1L);
        Assertions.assertThat(savedTask2.getId()).isEqualTo(2L);
    }
}
