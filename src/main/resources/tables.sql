CREATE DATABASE tasks_management;

CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       name VARCHAR(255),
                       last_name VARCHAR(255),
                       is_admin BOOLEAN
);

CREATE TABLE tasks (
                       id SERIAL PRIMARY KEY,
                       task_name TEXT
);

CREATE TABLE task_assigned (
                               id SERIAL PRIMARY KEY,
                               is_completed BOOLEAN,
                               user_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
                               task_id INTEGER REFERENCES tasks(id) ON DELETE CASCADE
);