package com.ronaldo.techgap.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.web.bind.annotation.CrossOrigin;


import static com.ronaldo.techgap.enums.Permission.*;
import static com.ronaldo.techgap.enums.Role.ADMIN;
import static com.ronaldo.techgap.enums.Role.USER;
import static org.springframework.http.HttpMethod.*;

@Configuration
@CrossOrigin(origins = "http://localhost:3000")
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;
    private final LogoutHandler logoutHandler;


    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception{

        httpSecurity
                .csrf(AbstractHttpConfigurer::disable)
                .sessionManagement(session -> session
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests(authorize -> authorize

                        .requestMatchers("api/v1/auth/**").permitAll()  // ok

                        .requestMatchers("api/v1/users/fetch-user").hasAnyRole(ADMIN.name(), USER.name())
                        .requestMatchers("api/v1/users/**").hasRole(ADMIN.name())

                        .requestMatchers("api/v1/tasks/fetch-tasks").hasRole(ADMIN.name())
                        .requestMatchers("api/v1/tasks/create").hasRole(ADMIN.name())
                        .requestMatchers("api/v1/tasks/delete/**").hasRole(ADMIN.name())
                        .requestMatchers("api/v1/tasks/**").hasAnyRole(ADMIN.name(), USER.name())

                        .anyRequest().authenticated()
                )
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .logout(logout -> logout.logoutUrl("/api/v1/auth/logout")
                .addLogoutHandler(logoutHandler)
                .logoutSuccessHandler(
                        (request, response, authentication) ->
                        SecurityContextHolder.clearContext())
                );
        return httpSecurity.build();
    }

}
