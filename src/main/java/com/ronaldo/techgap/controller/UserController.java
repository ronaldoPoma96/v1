package com.ronaldo.techgap.controller;

import com.ronaldo.techgap.dto.TaskAssignedDTO;
import com.ronaldo.techgap.dto.TokenDTO;
import com.ronaldo.techgap.entity.User;
import com.ronaldo.techgap.service.ITasksAssigned;
import com.ronaldo.techgap.service.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController @AllArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/v1/users")
public class UserController {

    private IUserService iUserService;


    // ENDPOINTS

    @Operation(summary = "Get list of users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found list of users"),
            @ApiResponse(responseCode = "404", description = "Users not found")
    })
    @GetMapping("/fetch-users")
    public ResponseEntity<List<User>> getAllUsers (){

        return ResponseEntity.status(HttpStatus.OK).body(iUserService.findAllUsers());
    }

    @Operation(summary = "Get user by token")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found user"),
            @ApiResponse(responseCode = "404", description = "User not found")
    })
    @PostMapping("/fetch-user")
    public ResponseEntity<User> getUserByToken(@RequestBody TokenDTO token){

        return ResponseEntity.status(HttpStatus.OK).body(iUserService.findUserByToken(token));
    }



}
