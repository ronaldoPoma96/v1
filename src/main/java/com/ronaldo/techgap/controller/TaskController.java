package com.ronaldo.techgap.controller;

import com.ronaldo.techgap.dto.ResponseDTO;
import com.ronaldo.techgap.dto.TaskAssignedDTO;
import com.ronaldo.techgap.dto.TaskAssignmentDTO;
import com.ronaldo.techgap.entity.Task;
import com.ronaldo.techgap.entity.User;
import com.ronaldo.techgap.service.ITaskService;
import com.ronaldo.techgap.service.ITasksAssigned;
import com.ronaldo.techgap.service.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/v1/tasks")
public class TaskController {

    private ITaskService iTaskService;
    private ITasksAssigned iTasksAssigned;

    // ENDPOINTS:

    @Operation(summary = "Get list of tasks")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found list of tasks"),
            @ApiResponse(responseCode = "404", description = "tasks not found")
    })
    @GetMapping("/fetch-tasks")
    public ResponseEntity<List<Task>> getAllTasks (){

        return ResponseEntity.status(HttpStatus.OK).body(iTaskService.findAllTasks());
    }

    @Operation(summary = "Get list of tasks by user id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found list of assigned tasks"),
            @ApiResponse(responseCode = "404", description = "Tasks not found")
    })
    @GetMapping("/fetch/task/{userId}")
    public ResponseEntity<List<TaskAssignedDTO>> getAllTasksByUserId (@PathVariable Integer userId){

        List<TaskAssignedDTO> taskAssignedDTOList = iTasksAssigned.getAllTasksByUserId(userId);

        if(taskAssignedDTOList.size() > 0){
            return ResponseEntity.status(HttpStatus.OK).body(taskAssignedDTOList);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


    @Operation(summary = "Create a new task if user is an admin")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created task successfully"),
            @ApiResponse(responseCode = "417", description = "Create operation failed")
    })
    @PostMapping("/create")
    public ResponseEntity<ResponseDTO> createTask(@RequestBody TaskAssignmentDTO taskAssignmentDTO){

            if (taskAssignmentDTO.getTaskName() == null || taskAssignmentDTO.getTaskName().isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseDTO("400", "Il nome del task è obbligatorio"));

            } else{
                Task savedTask = iTaskService.createTask(taskAssignmentDTO.getTaskName());
                if(taskAssignmentDTO.getUserId() != null){
                    iTasksAssigned.assignTask(taskAssignmentDTO.getUserId(), savedTask);

                    return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseDTO("200", "Task creato e assegnato."));
                }
                return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseDTO("200", "Task creato e non assegnato."));
            }

    }


    @Operation(summary = "Update an assigned task to complete/incomplete")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated task successfully"),
            @ApiResponse(responseCode = "417", description = "Update operation failed")
    })
    @PutMapping("/update/task")
    public ResponseEntity<ResponseDTO> updateTask (@RequestBody TaskAssignedDTO taskAssignedDTO){

        boolean isUpdated = iTasksAssigned.updateTask(taskAssignedDTO);

        if(isUpdated){
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ResponseDTO("200", "Request processed successfully"));
        }

        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                .body(new ResponseDTO("417", "Update operation failed"));
    }


    @Operation(summary = "Delete a non-assigned task if user is an admin")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted task successfully"),
            @ApiResponse(responseCode = "409", description = "Delete operation failed: task already assigned"),
            @ApiResponse(responseCode = "400", description = "Delete operation failed: you are not an admin")
    })
    @DeleteMapping("/delete/{taskId}")
    public ResponseEntity<ResponseDTO> deleteTask (@PathVariable Long taskId){

            boolean isDeleted = iTasksAssigned.deleteTask(taskId);

            if (isDeleted) {
                return ResponseEntity.status(HttpStatus.OK)
                        .body(new ResponseDTO("200", "Request processed successfully"));
            }

            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(new ResponseDTO("409", "Delete operation failed: task already assigned"));
    }
}
