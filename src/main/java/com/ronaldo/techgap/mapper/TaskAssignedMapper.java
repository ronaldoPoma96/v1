package com.ronaldo.techgap.mapper;

import com.ronaldo.techgap.dto.TaskAssignedDTO;
import com.ronaldo.techgap.entity.TaskAssigned;

public class TaskAssignedMapper {

    public static TaskAssignedDTO mapToDTO(TaskAssigned taskAssigned) {
        TaskAssignedDTO taskAssignedDTO = new TaskAssignedDTO();
        taskAssignedDTO.setId(taskAssigned.getId());
        taskAssignedDTO.setCompleted(taskAssigned.isCompleted());
        taskAssignedDTO.setTaskName(taskAssigned.getTask().getTaskName());
        taskAssignedDTO.setUserId(taskAssigned.getUser().getId());
        taskAssignedDTO.setName(taskAssigned.getUser().getFirstName());
        taskAssignedDTO.setLastName(taskAssigned.getUser().getLastName());
        return taskAssignedDTO;
    }
}
