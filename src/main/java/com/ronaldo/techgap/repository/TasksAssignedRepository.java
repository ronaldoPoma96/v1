package com.ronaldo.techgap.repository;

import com.ronaldo.techgap.entity.TaskAssigned;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TasksAssignedRepository extends JpaRepository<TaskAssigned, Integer> {

    List<TaskAssigned> findAllByUserId(Integer userId);

    Optional<List<TaskAssigned>> findAllByTaskId(Long taskId);

}
