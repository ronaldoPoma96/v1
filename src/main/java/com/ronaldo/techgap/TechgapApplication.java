package com.ronaldo.techgap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechgapApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechgapApplication.class, args);
	}

}
