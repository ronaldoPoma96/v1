package com.ronaldo.techgap.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TaskAssignedDTO {

    private Integer id;

    private boolean isCompleted;

    private String taskName;

    private Integer userId;

    private String name;

    private String lastName;

    private boolean isAdmin;

}
