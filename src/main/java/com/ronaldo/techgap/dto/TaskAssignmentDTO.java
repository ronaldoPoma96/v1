package com.ronaldo.techgap.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskAssignmentDTO {

    private String taskName;

    private Integer userId;
}
