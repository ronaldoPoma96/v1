package com.ronaldo.techgap.service.impl;

import com.ronaldo.techgap.entity.Task;
import com.ronaldo.techgap.repository.TaskRepository;
import com.ronaldo.techgap.service.ITaskService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service @AllArgsConstructor
public class TaskServiceImpl implements ITaskService {

    private TaskRepository taskRepository;


    @Override
    public Task createTask(String taskName) {
        Task task = new Task();
        task.setTaskName(taskName);
        task = taskRepository.save(task);

        return task;
    }

    @Override
    public List<Task> findAllTasks() {
        return taskRepository.findAll();
    }
}
