package com.ronaldo.techgap.service.impl;

import com.ronaldo.techgap.dto.TaskAssignedDTO;
import com.ronaldo.techgap.entity.Task;
import com.ronaldo.techgap.entity.TaskAssigned;
import com.ronaldo.techgap.entity.User;
import com.ronaldo.techgap.mapper.TaskAssignedMapper;
import com.ronaldo.techgap.repository.TaskRepository;
import com.ronaldo.techgap.repository.TasksAssignedRepository;
import com.ronaldo.techgap.repository.UserRepository;
import com.ronaldo.techgap.service.ITasksAssigned;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TasksAssignedImpl implements ITasksAssigned {

    private TasksAssignedRepository tasksAssignedRepository;
    private TaskRepository taskRepository;
    private UserRepository userRepository;


    @Override
    public List<TaskAssignedDTO> getAllTasksByUserId(Integer userId) {

        return tasksAssignedRepository.findAllByUserId(userId)
                .stream().map(TaskAssignedMapper::mapToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void assignTask(Integer userId, Task savedTask) {

        TaskAssigned taskAssigned = new TaskAssigned();
        Optional<User> userTemp = userRepository.findById(userId);

        if(userTemp.isPresent()){
            User user = userTemp.get();

            taskAssigned.setTask(savedTask);
            taskAssigned.setUser(user);
            taskAssigned.setCompleted(false);

            tasksAssignedRepository.save(taskAssigned);
        }
    }

    @Override
    public boolean updateTask(TaskAssignedDTO taskAssignedDTO) {

        Optional<TaskAssigned> taskAssignedTemp = tasksAssignedRepository.findById(taskAssignedDTO.getId());

        if(taskAssignedTemp.isPresent()){

            TaskAssigned taskAssigned = taskAssignedTemp.get();
            taskAssigned.setCompleted(taskAssignedDTO.isCompleted());
            tasksAssignedRepository.save(taskAssigned);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteTask(Long taskId) {

        Optional<List<TaskAssigned>> taskAssignedListByTaskId = tasksAssignedRepository.findAllByTaskId(taskId);
        if(taskAssignedListByTaskId.get().size() > 0){
            return false;
        }

        Optional<Task> taskOptional = taskRepository.findById(taskId);

        if(taskOptional.isPresent()){
            taskRepository.deleteById(taskId);
            return true;
        }
        return false;
    }


}
