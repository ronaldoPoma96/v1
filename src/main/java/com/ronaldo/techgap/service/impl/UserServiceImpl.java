package com.ronaldo.techgap.service.impl;

import com.ronaldo.techgap.config.JwtService;
import com.ronaldo.techgap.dto.TokenDTO;
import com.ronaldo.techgap.entity.User;
import com.ronaldo.techgap.repository.UserRepository;
import com.ronaldo.techgap.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service @AllArgsConstructor
public class UserServiceImpl implements IUserService {

    UserRepository userRepository;
    JwtService jwtService;


    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUserByToken(TokenDTO token) {

        System.out.println("Dentro findUserByToken: ");

        String email = jwtService.extractUsername(token.getToken());
        Optional<User> userStored = userRepository.findByEmail(email);
        if(userStored.isPresent()){
            return userStored.get();
        }

        // Gestire nel caso User non esiste
        return null;
    }
}
