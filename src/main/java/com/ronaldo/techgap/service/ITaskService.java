package com.ronaldo.techgap.service;

import com.ronaldo.techgap.entity.Task;

import java.util.List;

public interface ITaskService {

    Task createTask(String taskName);

    List<Task> findAllTasks ();
}
