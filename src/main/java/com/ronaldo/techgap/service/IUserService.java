package com.ronaldo.techgap.service;

import com.ronaldo.techgap.dto.TokenDTO;
import com.ronaldo.techgap.entity.User;

import java.util.List;

public interface IUserService {

    List<User> findAllUsers();

    User findUserByToken(TokenDTO token);
}
