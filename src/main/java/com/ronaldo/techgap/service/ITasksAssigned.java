package com.ronaldo.techgap.service;

import com.ronaldo.techgap.dto.TaskAssignedDTO;
import com.ronaldo.techgap.entity.Task;
import com.ronaldo.techgap.entity.TaskAssigned;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ITasksAssigned {

    List<TaskAssignedDTO> getAllTasksByUserId (Integer userId);

    void assignTask(Integer userId, Task savedTask);

    boolean updateTask(TaskAssignedDTO taskAssignedDTO);

    boolean deleteTask(Long taskId);

}
